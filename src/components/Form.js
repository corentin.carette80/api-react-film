import React, { useEffect, useState } from "react";
//18:22
import axios from "axios";
import Card from "./Card";

//56:35
const Form = () => {
  const [moviesData, setMoviesData] = useState([]);
  const [cherche, setcherche] = useState("code");
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=67636b98a985573f3215e98bb953fbe4&query=${cherche}&language=fr-FR`
      )
      .then((res) => setMoviesData(res.data.results));
  }, [cherche]);
  return (
    <div className="form-component">
      <div className="form-container">
        <form>
          <input
            type="text"
            placeholder="entrez le titre d'un film"
            id="search-input"
            onChange={(e) => setcherche(e.target.value)}
          />
          {/* <input type="submit" value="Rechercher"/> */}
        </form>
      </div>
      <div className="result">
        {moviesData.map((movie) => (
          <Card movie={movie} key={movie.id} />
        ))}
      </div>
    </div>
  );
};

export default Form;
