import React from "react";
import { NavLink } from "react-router-dom";


const Header = () => {
    return (
       <div className="Header">
           <nav>
               <ul>
                   <NavLink to="/"><li>accueil</li></NavLink>
                   <NavLink to="/favorie"><li>favorie</li></NavLink>
               </ul>
           </nav>
           <h1 className="titre">Film</h1>
       </div>
       
       )
  
  }
  export default Header;