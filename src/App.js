import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import "./styles/style.scss";
import Home from "./pages/Home";
import UserList from "./pages/UserList";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/favorie" element={<UserList />} />
        <Route path="*" element={<Home />} />
      </Routes>
    </BrowserRouter>
  );
};
export default App;
//     <div className="App">

//       <Form/>
//       {/* <Film title='Top film' />
//       <input type="text" className='recherche'/> */}
//       {/* <div className='flex'>
//         { this.state.film ? (this.state.film.map((film)  => { return (
//         <Fragment >
//           <div className='affiche' key={film.id}>
//             <p>{film.title}</p>
//             <img className='petitimg' src= {"https://image.tmdb.org/t/p/w500" + film.poster_path}></img>
//             <button onClick={() =>{localStorage.setItem('favorie'+film.id, film.title)}}>Add</button>

//             <button  onClick={() =>{localStorage.removeItem('favorie'+film.id)}}>Remove</button><br/>

//             <a href={"https://www.themoviedb.org/movie/" + film.id}>details</a>
//           </div>
//         </Fragment>
//         ) })) : 'rien'  }
//       </div> */}
//     </div>
//   );
// }
// }

//pour favorie localStorage ?
