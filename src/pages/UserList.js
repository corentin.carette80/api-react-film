import axios from "axios";
import React from "react";
import { useEffect, useState } from "react/cjs/react.development";
import Header from "../components/Header";
import Card from "../components/Card";

const UserList = () => {
  const [listData, setListData] = useState([]);
  useEffect(() => {
    let moviesId = window.localStorage.movies
      ? window.localStorage.movies.split(",")
      : [];
    for (let i = 0; i < moviesId.length; i++) {
      axios
        .get(
          `https://api.themoviedb.org/3/movie/${moviesId[i]}?api_key=67636b98a985573f3215e98bb953fbe4&language=fr-FR`
        )
        .then((res) => setListData((listData) => [...listData, res.data]));
    }
  }, []);
  return (
    <div>
      <Header />
      <h2 className="titre">favorie</h2>
      <div className="result">
        {listData.length > 0 ? (
          listData.map((movie) => <Card movie={movie} key={movie.id} />)
        ) : (
          <h2>pas de favorie</h2>
        )}
      </div>
    </div>
  );
};
export default UserList;
